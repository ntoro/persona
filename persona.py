class Persona:
    def __init__(self, nombre, apellidos, fecha_nacimiento, DNI):
        self.nombre = nombre
        self.apellidos = apellidos
        self.fecha_nacimiento = fecha_nacimiento  #get base cambia el valor de la base definida
        self.DNI = DNI


    def setNombre(self, nombre):
        self._nombre = nombre

    def getNombre(self):
        return self._nombre

    def setApellidos(self, apellidos):
        self._apellidos = apellidos
    
    def getApellidos(self):
        return self._apellidos

    def setFecha_nacimiento(self, fecha_nacimiento):
        self._fecha_nacimiento = fecha_nacimiento

    def getFecha_nacimiento(self):
        return self._fecha_nacimiento

    def setDNI(self, DNI):
        self._DNI = DNI

    def getDNI(self):
        return self._DNI

    def __str__(self):
        return 'El paciente {nombre} {apellidos}, su fecha de nacimiento {fecha_nacimiento} y con DNI: {DNI}'.format(nombre= self.nombre, apellidos = self.apellidos, fecha_nacimiento= self.fecha_nacimiento, DNI = self.DNI )

    

class Paciente(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, DNI, historial_clinico):
        super().__init__(nombre, apellidos, fecha_nacimiento, DNI)
        self.historial_clinico = historial_clinico

    def VerHistorialClinico(self, historial_clinico):
        return self.historial_clinico
    
    #def __str__(self):
        #return 'El paciente {nombre} {apellidos}, su fecha de nacimiento {fecha_nacimiento}, con DNI: {DNI}, y su historia clínica es: {historial_clinico}'.format(nombre= self.nombre, apellidos = self.apellidos, fecha_nacimiento= self.fecha_nacimiento, DNI = self.DNI, historial_clinico = self.historial_clinico)

class Medico(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, DNI, especialidad, citas):
        super().__init__(nombre, apellidos, fecha_nacimiento, DNI)
        self.especialidad = especialidad
        self.citas = citas

    def ConsultarAgenda(self, especialidad, citas):
        return (self.especialidad, self.citas)

    #def __str__(self):
        #return 'El paciente {nombre} {apellidos}, su fecha de nacimiento {fecha_nacimiento},con DNI: {DNI}, tratado en la/s especialidad/es : {especialidad}, y con cita/s : {citas}'.format(nombre= self.nombre, apellidos = self.apellidos, fecha_nacimiento= self.fecha_nacimiento, DNI = self.DNI, especialidad = self.especialidad, citas = self.citas)
if __name__ == '__main__':
    paciente01 = Paciente('Sofía', 'López Arias', '05/10/2003', '14132580J', 'Ansiedad')
    print(paciente01)
    medico01 = Medico('Julia', 'Serrano Méndez', '13/09/1988', '78965214S', 'Dermatología', '07/04/2024')
    print(medico01)
