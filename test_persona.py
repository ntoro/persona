from persona import Persona, Paciente, Medico
import unittest


class PersonaTestClase(unittest.TestCase):
    def test_VerHistorialClinico(self):
        paciente01 = Paciente('Sofía', 'López Arias', '05/10/2003', '14132580J', 'Ansiedad')
        historial_clinico = paciente01.VerHistorialClinico()
        self.assertEqual(historial_clinico, 'Ansiedad')
    
    def test_ConsultarAgenda(self):
        medico01 = Medico('Julia', 'Serrano Méndez', '13/09/1988', '78965214S', 'Dermatología', '07/04/2024')
        especialidad, citas = medico01.ConsultarAgenda()
        self.assertEqual(especialidad, citas, ('Dermatología','07/04/2024'))


#if __name__ == '__main__':       
    unittest.main()
